ARG MY_PATH
FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY $MY_PATH ./
ENTRYPOINT ["dotnet", "aspnetapp.dll"]
